/*
 -----------------------------------------------------------------------------------
 Bachelor   :  Instant Messaging Platfrom
 Project    :  Simulating a chat bot that communicate with mattermost outgoing webhoks
 File       :  ChatbotServer.java
 Author     :  Nanchen Lionel
 Responsible:  Fatemi Nastaran
 Start-up   :  Dermintel (Anthony Hessab)
 Date       :  06.03.2019

 Goal       :

 Compiler : JDK 1.8
 -----------------------------------------------------------------------------------
*/

import com.google.gson.Gson;
import com.sun.net.httpserver.*;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.*;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class ChatBot {
    final private String ACCESS_TOKEN = "ux4charjit8dfn31dnz568bqea";
    final private String BOT_ID = "jiut8t3jntfupk93tphidraywe";
    private Gson gson = new Gson();
    private Random random = new Random();
    private JSONArray users;

    /**
     * main
     * @param args
     */
    public static void main(String[] args) {
        new ChatBot();
    }

    /**
     * constructor - start the server
     */
    public ChatBot() {
        try {

            JSONParser parser = new JSONParser();

            this.users = (JSONArray) parser.parse(new FileReader("./src/users.json"));

            InetSocketAddress address = new InetSocketAddress(8060);

            HttpServer server = HttpServer.create(address, 0);

            System.out.println("Listening on http://localhost:8060");
            server.createContext("/signup").setHandler(this::handleSignup);
            server.createContext("/user-access-token").setHandler(this::handleUserAccessToken);
            server.createContext("/outgoing-webhooks").setHandler(this::handleOutgoingWebhooks);
            server.createContext("/buttons").setHandler(this::handleButtons);
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * handle the request to signup a new user
     * API: /signup
     * @param exchange the request received
     * @throws IOException
     */
    private void handleSignup(HttpExchange exchange) throws IOException {
        System.out.println("\n --- Signup request received ---");

        //get the JSON payload
        Scanner in = new Scanner(exchange.getRequestBody(), "UTF-8");
        String json = "";
        while(in.hasNextLine()) {
            json += in.nextLine();
        }
        in.close();

        //transform the json into a map
        Map<String, String> map = gson.fromJson(json, Map.class);

        String username = map.get("username");
        String email = map.get("email");
        String password = map.get("password");

        //create user
        String userId = this.createNewUser(username, email, password);
        System.out.println("User id: " + userId);

        //check that the user doesn't already exists
        if (!userId.contains("sql")) {
            //create team
            String teamId = this.createNewTeam(username);
            System.out.println("Team id: " + teamId);

            //create channel
            String channelId = this.createNewChannel(teamId);
            System.out.println("Channel id: " + channelId);

            //create outgoing webhook
            String webhookToken = this.createNewWebhook(username, channelId, teamId);
            System.out.println("Webhook token: " + webhookToken);

            boolean success = this.addUserAndBotToTeam(userId, teamId);
            System.out.println("Add user and bot to team: " + success);

            if (success) {
                success = this.addUserToChannel(userId, channelId, teamId);
                System.out.println("Add user to channel: " + success);

                if (success) {
                    success = this.addUserToChannel(BOT_ID, channelId, teamId);
                    System.out.println("Add bot to channel: " + success);

                    if (success) {
                        String createdAccessToken = this.createAccessToken(userId, username);
                        System.out.println("User access token: " + createdAccessToken);
                        success = createdAccessToken != null;

                        if (success) {
                            success = this.clearChannel(channelId);

                            if (success) {
                                //create the new user as JSON
                                JSONObject newUser = new JSONObject();
                                newUser.put("username", username);
                                newUser.put("id", userId);
                                newUser.put("team_id", teamId);
                                newUser.put("channel_id", channelId);
                                newUser.put("webhook_token", webhookToken);
                                newUser.put("user_access_token", createdAccessToken);

                                users.add(newUser);

                                try (FileWriter file = new FileWriter("./src/users.json")) {
                                    file.write(users.toJSONString());
                                    file.flush();
                                    file.close();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
            }

            this.sendResponse(exchange, "{\"success\": \"" + (success ? "success" : "failed") + "\"}");
        } else {
            this.sendResponse(exchange, "{\"success\": \"taken\"}");
        }
    }

    /**
     * handle the request to get a user access token
     * API: /user-access-token
     * @param exchange the request received
     * @throws IOException
     */
    private void handleUserAccessToken(HttpExchange exchange) throws IOException {
        System.out.println("\n --- Request for user access token ---");

        //get the JSON payload
        Scanner in = new Scanner(exchange.getRequestBody(), "UTF-8");
        String json = "";
        while(in.hasNextLine()) {
            json += in.nextLine();
        }
        in.close();

        //transform the json into a map
        Map<String, String> map = gson.fromJson(json, Map.class);

        String id = map.get("user_id");
        String token = "";
        for (Object object: users) {
            JSONObject user = (JSONObject) object;
            if (id.equals((String) user.get("id"))) {
                token = (String) user.get("user_access_token");
                System.out.println("Token: " + token);
            }
        }

        this.sendResponse(exchange, "{\"token\": \"" + token + "\"}");
    }

    /**
     * handle the request that come from a outgoing webhooks
     * API: /outgoing-webhooks
     * @param exchange the request received
     * @throws IOException
     */
    private void handleOutgoingWebhooks(HttpExchange exchange) throws IOException {
        System.out.println("\n --- Outgoing webhook received ---");

        //get the JSON payload
        Scanner in = new Scanner(exchange.getRequestBody(), "UTF-8");
        String json = "";
        while(in.hasNextLine()) {
            json += in.nextLine();
        }
        in.close();
        //transform the json into a map
        Map<String, String> map = gson.fromJson(json, Map.class);

        //get the channel id so we can know to wich user we talk
        String channelId = map.get("channel_id");

        //get and check the token if ok send a response otherwise display an error message
        String token = map.get("token");
        String webhookToken = "";

        for (Object object: users) {
            JSONObject user = (JSONObject) object;
            if (channelId.equals((String) user.get("channel_id"))) {
                webhookToken = (String) user.get("webhook_token");
                break;
            }
        }

        if (token != null && token.equals(webhookToken)) {
            //return if the user id is the one from the chatbot account
            if (map.get("user_id").equals(BOT_ID)) { System.out.println("Ignored because come from chatbot account"); return; }
            String message = map.get("text");
            String response = "";

            System.out.println("- Message from " + map.get("user_name") + ": \"" + map.get("text") + "\"");

            //simulate waiting
            int seconds = random.nextInt(4);
            System.out.println("- Waiting " + seconds + " seconds");
            try {
                TimeUnit.SECONDS.sleep(seconds);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //check if a location has been sent
            if (message.startsWith("https://www.google.com/map")) {
                response = googleMapReceived(message);
            } else {
                switch (message.toLowerCase()) {
                    case "small text":
                        response = smallText();
                        break;
                    case "normal text":
                        response = normalText();
                        break;
                    case "long text": //----------
                        response = longText();
                        break;
                    case "image": //----------
                        response = "";
                        try {
                            image(channelId);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case "image url":
                        response = imageUrl("https://images-na.ssl-images-amazon.com/images/I/61dCcxXxZpL._SL1015_.jpg", "https://miro.medium.com/max/1400/1*vdJH8HDhqmEw5-2wEqNj8g.jpeg");
                        break;
                    case "button": //----------
                        response = button();
                        break;
                    case "link":
                        response = link();
                        break;
                    case "youtube link":
                        response = youtubeLink();
                        break;
                    case "location":
                        response = googleMap();
                        break;
                    case "ciao":
                        response = disconnect();
                        break;
                    default:
                        response = defaultText();
                        break;
                }
            }

            sendResponse(exchange, response);
            System.out.println();
        } else {
            System.out.println("Wrong token, so wrong outgoing webhook !");
        }
    }

    /**
     * handle the request that come from a outgoing webhooks
     * API: /buttons
     * @param exchange the request received
     * @throws IOException
     */
    private void handleButtons(HttpExchange exchange) throws IOException {
        //get the JSON payload
        Scanner in = new Scanner(exchange.getRequestBody(), "UTF-8");
        String json = "";
        while(in.hasNextLine()) {
            json += in.nextLine();
        }
        in.close();
        //transform the json into a map
        Map<String, Object> map = gson.fromJson(json, Map.class);
        Map<Object, Object> context = gson.fromJson(map.get("context").toString(), Map.class);
        String postId = (String) map.get("post_id");
        String channelId = (String) map.get("channel_id");

        String text = recoverText(postId);

        if (context.get("action").equals("yes")) {
            System.out.println("Button choosed: Yes");
            sendResponse(exchange, "{\"update\": {\"message\": \"" + text + "\"}}");
            sendButtonResponse("You choosed Yes", channelId);
        } else if (context.get("action").equals("no")) {
            System.out.println("Button choosed: No");
            sendResponse(exchange, "{\"update\": {\"message\": \"" + text + "\"}}");
            sendButtonResponse("You choosed No", channelId);
        }
    }

    private String recoverText(String postId) {
        String text = "";
        try {
            CloseableHttpClient client = HttpClients.createDefault();
            HttpGet httpGet = new HttpGet("http://localhost:8065/api/v4/posts/" + postId);

            httpGet.setHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            httpGet.setHeader("Content-type", "application/json");

            CloseableHttpResponse response = client.execute(httpGet);

            Scanner sc = new Scanner(response.getEntity().getContent());

            String json = "";
            while (sc.hasNext()) {
                json = sc.nextLine();
            }

            Map<String, Object> map = gson.fromJson(json, Map.class);

            Map<String, Object> props = (Map<String, Object>) map.get("props");

            ArrayList attachments = (ArrayList) props.get("attachments");

            text = (String) ((Map<String, Object>) attachments.get(0)).get("text");

            client.close();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return text;
    }

    /**
     * Handle the response to a message's button
     * @param text the content of the response
     * @param channelId the channel's id to send the repsonse
     */
    private void sendButtonResponse(String text, String channelId) {
        try {
            CloseableHttpClient client = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost("http://localhost:8065/api/v4/posts");

            //props from_webhook is to indicate that this message comes from the chatbot
            String json = "{\n" +
                    "\"channel_id\": \"" + channelId + "\",\n" +
                    "\"message\": \"" + text + "\",\n" +
                    "\"props\": {\"from_webhook\": \"true\"}\n" +
                    "}";
            StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            httpPost.setHeader("Content-type", "application/json");

            CloseableHttpResponse response = client.execute(httpPost);
            client.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * stop the server
     */
    private String disconnect() {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("Disconnecting ...");
                System.exit(1);
            }
        }, 20);

        return "{\"text\": \"Have a good one!\"}";
    }

    /**
     * send a response
     * @param exchange the request
     * @param response the response
     * @throws IOException
     */
    private void sendResponse(HttpExchange exchange, String response) throws IOException {
        if (!response.isEmpty()) {
            exchange.sendResponseHeaders(200, response.getBytes().length);
            exchange.setAttribute("Content-Type", "application/json");
            OutputStream os = exchange.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    /**
     * print the request's infos
     * @param exchange the request to print
     */
    private void printRequestInfo(HttpExchange exchange) {
        System.out.println("-- headers --");
        Headers requestHeaders = exchange.getRequestHeaders();
        requestHeaders.entrySet().forEach(System.out::println);

        System.out.println("-- HTTP method --");
        String requestMethod = exchange.getRequestMethod();
        System.out.println(requestMethod);

        System.out.println("-- JSON --");
        Scanner in = new Scanner(exchange.getRequestBody(), "UTF-8");
        String request = "";
        while(in.hasNextLine()) {
            request += in.nextLine();
        }
        System.out.println(request);
    }

    /**
     * Create a new user
     * @param username the user's name
     * @param password the user's password
     * @param email the user's email
     * @return the team's id
     */
    private String createNewUser(String username, String email, String password) {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost("http://localhost:8065/api/v4/users");

        try {
            String json = "{\"email\":\"" + email + "\", \"username\": \"" + username + "\", \"password\": \"" + password + "\", \"notify_props\": { \"email\": \"false\" }}";
            StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            httpPost.setHeader("Content-type", "application/json");

            CloseableHttpResponse response = client.execute(httpPost);

            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            Map<String, Object> map = this.gson.fromJson(String.valueOf(result), Map.class);

            client.close();
            return (String) map.get("id");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Create a new team
     * @param username the user's name
     * @return the team's id
     */
    private String createNewTeam(String username) {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost("http://localhost:8065/api/v4/teams");

        try {
            String json = "{\"name\":\"team-" + username.toLowerCase() + "\", \"display_name\": \"Team-" + username + "\", \"type\": \"I\"}";
            StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            httpPost.setHeader("Content-type", "application/json");

            CloseableHttpResponse response = client.execute(httpPost);

            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            Map<String, Object> map = this.gson.fromJson(String.valueOf(result), Map.class);

            client.close();
            return (String) map.get("id");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Create a new channel
     * @return the channel's id
     */
    private String createNewChannel(String teamId) {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost("http://localhost:8065/api/v4/channels");

        try {
            String json = "{\"team_id\": \"" + teamId + "\", \"name\": \"dermchat\", \"display_name\": \"Dermchat\", \"type\": \"O\"}";
            StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            httpPost.setHeader("Content-type", "application/json");

            CloseableHttpResponse response = client.execute(httpPost);

            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            Map<String, Object> map = this.gson.fromJson(String.valueOf(result), Map.class);

            client.close();
            return (String) map.get("id");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Create a new webhook for the channel
     * @param username the user's name
     * @param channelId the channel's id
     * @param teamId the team's id
     * @return the webhook's token
     */
    private String createNewWebhook(String username, String channelId, String teamId) {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost("http://localhost:8065/api/v4/hooks/outgoing");

        try {
            String json = "{\"team_id\": \"" + teamId + "\", \"channel_id\": \"" + channelId + "\", \"display_name\": \"" + "Webhook-" + username + "\", \"trigger_words\": [], \"callback_urls\": [\"http://docker.for.mac.localhost:8060/outgoing-webhooks\"], \"content_type\": \"application/json\"}";
            StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            httpPost.setHeader("Content-type", "application/json");

            CloseableHttpResponse response = client.execute(httpPost);

            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            Map<String, Object> map = this.gson.fromJson(String.valueOf(result), Map.class);

            client.close();
            return (String) map.get("token");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Add the new user and the bot to the new team
     * @param userId the user's id
     * @param teamId the team's id
     * @return a boolean indicating the success of the request
     */
    private boolean addUserAndBotToTeam(String userId, String teamId) {
        try {
            CloseableHttpClient client = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost("http://localhost:8065/api/v4/teams/" + teamId + "/members");

            String json = "{\"user_id\": \"" + userId + "\", \"team_id\": \"" + teamId + "\"}";
            StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            httpPost.setHeader("Content-type", "application/json");

            CloseableHttpResponse response = client.execute(httpPost);

            client.close();

            return response.getStatusLine().getStatusCode() == 201;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Add a user to the new channel
     * @param userId the user's id to add
     * @param channelId the channel's id
     * @param teamId the team's id
     * @return a boolean indicating the success of the request
     */
    private boolean addUserToChannel(String userId, String channelId, String teamId) {
        try {
            CloseableHttpClient client = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost("http://localhost:8065/api/v4/channels/" + channelId + "/members");

            String json = "{\"user_id\": \"" + userId + "\", \"team_id\": \"" + teamId + "\"}";
            StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            httpPost.setHeader("Content-type", "application/json");

            CloseableHttpResponse response = client.execute(httpPost);

            client.close();

            return response.getStatusLine().getStatusCode() == 201;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Create a user access token for a user
     * @param userId the user's id
     * @param username the user's name
     * @return a boolean indicating the success of the request
     */
    private String createAccessToken(String userId, String username) {
        try {
            CloseableHttpClient client = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost("http://localhost:8065/api/v4/users/" + userId + "/tokens");

            String json = "{\"description\": \"Personal access token for " + username + ".\"}";
            StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            httpPost.setHeader("Content-type", "application/json");

            CloseableHttpResponse response = client.execute(httpPost);

            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            Map<String, String> map = this.gson.fromJson(String.valueOf(result), Map.class);
            client.close();

            return map.get("token");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Clear the channel
     * When you add user to channel a message is created to indicate that a user has been added to the channel
     * This method delete the two messages
     * @param channelId the channel's id
     * @return true if the channel is successfully cleared, false otherwise
     */
    private boolean clearChannel(String channelId) {
        try {
            CloseableHttpClient client = HttpClients.createDefault();
            HttpGet httpGet = new HttpGet("http://localhost:8065/api/v4/channels/" + channelId + "/posts");

            httpGet.setHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            httpGet.setHeader("Content-type", "application/json");

            CloseableHttpResponse response = client.execute(httpGet);

            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            Map<String, Object> map = this.gson.fromJson(String.valueOf(result), Map.class);
            JSONObject json = new JSONObject(map);

            ArrayList<String> array = (ArrayList<String>) json.get("order");

            boolean success = false;

            for (String id : array) {
                HttpDelete httpDelete = new HttpDelete("http://localhost:8065/api/v4/posts/" + id);
                httpDelete.setHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                httpDelete.setHeader("Content-type", "application/json");

                response = client.execute(httpDelete);

                success = response.getStatusLine().getStatusCode() == 200;

                if (!success) break;
            }

            client.close();

            return success;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Create a JSON payload with a small text
     * @return the JSON payload
     */
    private String smallText() {
        System.out.println("Sending small text");

        return "{\"text\": \"That still only counts as one.\", \"icon_url\": \"https://res.cloudinary.com/teepublic/image/private/s--R9uL4Dv7--/t_Resized%20Artwork/c_fit,g_north_west,h_954,w_954/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_bbbbbb,e_outline:3:1000/c_mpad,g_center,h_1260,w_1260/b_rgb:eeeeee/c_limit,f_jpg,h_630,q_90,w_630/v1499156483/production/designs/1712341_0.jpg\"}";
    }

    /**
     * Create a JSON payload with a medium size text
     * @return the JSON payload
     */
    private String normalText() {
        System.out.println("Sending normal text");

        return "{\"text\": \"Home is behind, the world ahead... and there are many paths to tread... Through shadow... To the edge of night... Until the stars are all alight... Mist and shadow, cloud and shade... All shall fade... all... shall... fade.\", \"icon_url\": \"https://res.cloudinary.com/teepublic/image/private/s--R9uL4Dv7--/t_Resized%20Artwork/c_fit,g_north_west,h_954,w_954/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_bbbbbb,e_outline:3:1000/c_mpad,g_center,h_1260,w_1260/b_rgb:eeeeee/c_limit,f_jpg,h_630,q_90,w_630/v1499156483/production/designs/1712341_0.jpg\"}";
    }

    /**
     * Create a JSON payload with a long text
     * @return the JSON payload
     */
    private String longText() {
        System.out.println("Sending long text");

        return "{\"text\": \"And thus it was. A fourth age of middle-earth began. And the fellowship of the ring... though eternally bound by friendship and love... was ended. Thirteen months to the day since Gandalf sent us on our long journey... we found ourselves looking upon a familiar sight. We were home. How do you pick up the threads of an old life? How do you go on... when in your heart you begin to understand... there is no going back? There are some things that time cannot mend... some hurts that go too deep... that have taken hold. Bilbo once told me his part in this tale would end... that each of us must come and go in the telling. Bilbo's story was now over. There would be no more journeys for him... save one. My dear Sam. You cannot always be torn in two. You will have to be one and whole for many years. You have so much to enjoy and to be and to do. Your part in the story will go on.\", \"icon_url\": \"https://res.cloudinary.com/teepublic/image/private/s--R9uL4Dv7--/t_Resized%20Artwork/c_fit,g_north_west,h_954,w_954/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_bbbbbb,e_outline:3:1000/c_mpad,g_center,h_1260,w_1260/b_rgb:eeeeee/c_limit,f_jpg,h_630,q_90,w_630/v1499156483/production/designs/1712341_0.jpg\"}";
    }

    /**
     * Create a JSON payload with an image url and a text
     * @return the JSON payload
     */
    private String imageUrl(String linkA, String linkB) {
        System.out.println("Sending image url");

        String link = "";

        switch (this.random.nextInt(2)) {
            case 0:
                link = linkA;
                break;
            case 1:
                link = linkB;
                break;
        }

        return "{\n" + randomText() +
                "\"icon_url\": \"https://res.cloudinary.com/teepublic/image/private/s--R9uL4Dv7--/t_Resized%20Artwork/c_fit,g_north_west,h_954,w_954/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_bbbbbb,e_outline:3:1000/c_mpad,g_center,h_1260,w_1260/b_rgb:eeeeee/c_limit,f_jpg,h_630,q_90,w_630/v1499156483/production/designs/1712341_0.jpg\",\n" +
                "\"attachments\": [\n" +
                "{\n" +
                "\"image_url\": \"" + link + "\"" +
                "}\n" +
                "]\n" +
                "}";
    }

    /**
     * Create a JSON payload with an image and a text
     * @return the JSON payload
     */
    private void image(String channelId) throws Exception {
        String fileId = this.uploadImage(channelId);

        if (fileId == null) return;

        System.out.println("Sending image");

        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost("http://localhost:8065/api/v4/posts");

        try {
            String json = "{\n" +
                    "\"channel_id\": \"" + channelId + "\",\n" +
                    "\"message\": \"Image\",\n" +
                    "\"icon_url\": \"https://res.cloudinary.com/teepublic/image/private/s--R9uL4Dv7--/t_Resized%20Artwork/c_fit,g_north_west,h_954,w_954/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_bbbbbb,e_outline:3:1000/c_mpad,g_center,h_1260,w_1260/b_rgb:eeeeee/c_limit,f_jpg,h_630,q_90,w_630/v1499156483/production/designs/1712341_0.jpg\",\n" +
                    "\"file_ids\": [\"" + fileId + "\"],\n" +
                    "\"props\": {\"from_webhook\": \"true\"}\n" +
                    "}";
            StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            httpPost.setHeader("Content-type", "application/json");

            CloseableHttpResponse response = client.execute(httpPost);

            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            System.out.println(result.toString());

            client.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Upload an image to the Mattermost server
     * @param channelId the channel'id in which upload the image
     * NOT WORKING
     */
    private String uploadImage(String channelId) {
        System.out.println("Upload image");

        try {
            //get the image file
            String filename = "image_" + ((Integer) this.random.nextInt(4)).toString() + ".jpg";

            File file = new File("./src/ressources/" + filename);

            CloseableHttpClient client = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost("http://localhost:8065/api/v4/files");

            HttpEntity entity = MultipartEntityBuilder
                    .create()
                    .setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
//                    .addPart("channel_id", new StringBody(channelId))
                    .addTextBody("channel_id", channelId)
//                    .addPart("files", new FileBody(file))
                    .addBinaryBody("files", file, ContentType.DEFAULT_BINARY, file.getName())
                    .build();
            httpPost.setEntity(entity);
            httpPost.setHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            httpPost.setHeader("Content-type", "multipart/form-data");

            CloseableHttpResponse response = client.execute(httpPost);

            BufferedReader rd = new BufferedReader(
                    new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            System.out.println(result.toString().replace(",", ",\n"));

            Map<String, String> map = this.gson.fromJson(String.valueOf(result), Map.class);
            JSONObject json = new JSONObject(map);

            ArrayList<Map<String, Object>> array = (ArrayList<Map<String, Object>>) json.get("file_infos");

            String id = (String) array.get(0).get("id");

            client.close();

            return id;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Create a JSON payload with buttons and a text
     * @return the JSON payload
     */
    private String button() {
        System.out.println("Sending buttons");
        return
                "{\n" +
                        "  \"username\": \"Dermintel\",\n" +
                        "  \"icon_url\": \"https://res.cloudinary.com/teepublic/image/private/s--R9uL4Dv7--/t_Resized%20Artwork/c_fit,g_north_west,h_954,w_954/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_bbbbbb,e_outline:3:1000/c_mpad,g_center,h_1260,w_1260/b_rgb:eeeeee/c_limit,f_jpg,h_630,q_90,w_630/v1499156483/production/designs/1712341_0.jpg\",\n" +
                        "  \"attachments\": [\n" +
                        "    {\n" +
                        "      \"text\": \"Choose between one of the two buttons\",\n" +
                        "      \"actions\": [\n" +
                        "        {\n" +
                        "          \"name\": \"Yes\",\n" +
                        "          \"integration\": {\n" +
                        "            \"url\": \"http://docker.for.mac.localhost:8060/buttons\",\n" +
                        "            \"context\": {\n" +
                        "              \"action\": \"yes\",\n" +
                        "              \"type\": \"ephemeral\"\n" +
                        "            }\n" +
                        "          }\n" +
                        "        }, {\n" +
                        "          \"name\": \"No\",\n" +
                        "          \"integration\": {\n" +
                        "            \"url\": \"http://docker.for.mac.localhost:8060/buttons\",\n" +
                        "            \"context\": {\n" +
                        "              \"action\": \"no\",\n" +
                        "              \"type\": \"update\"\n" +
                        "            }\n" +
                        "          }\n" +
                        "        }\n" +
                        "      ]\n" +
                        "    }\n" +
                        "  ]\n" +
                        "}";
    }

    /**
     * Create a JSON payload with a link
     * @return the JSON payload
     */
    private String link() {
        System.out.println("Sending link");

        return "{\"text\": \"https://heig-vd.ch\", \"icon_url\": \"https://res.cloudinary.com/teepublic/image/private/s--R9uL4Dv7--/t_Resized%20Artwork/c_fit,g_north_west,h_954,w_954/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_bbbbbb,e_outline:3:1000/c_mpad,g_center,h_1260,w_1260/b_rgb:eeeeee/c_limit,f_jpg,h_630,q_90,w_630/v1499156483/production/designs/1712341_0.jpg\"}";
    }

    /**
     * Create a JSON payload with a YouTube link
     * @return the JSON payload
     */
    private String youtubeLink() {
        System.out.println("Sending Youtube link");

        return "{\"text\": \"https://www.youtube.com/watch?v=EzynjoF1Lw8\", \"icon_url\": \"https://res.cloudinary.com/teepublic/image/private/s--R9uL4Dv7--/t_Resized%20Artwork/c_fit,g_north_west,h_954,w_954/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_bbbbbb,e_outline:3:1000/c_mpad,g_center,h_1260,w_1260/b_rgb:eeeeee/c_limit,f_jpg,h_630,q_90,w_630/v1499156483/production/designs/1712341_0.jpg\"}";
    }

    /**
     * Create a JSON payload that respond to a message containing a Google Maps URL
     * @return the JSON payload
     */
    private String googleMapReceived(String url) {
        System.out.println("Sending Google Map response");

        String[] split = url.split("loc:");
        String[] coordinates = split[split.length - 1].split(Pattern.quote("+"));

        System.out.println(coordinates);

        double latitude = Double.parseDouble(coordinates[0]);
        double longitude = Double.parseDouble(coordinates[1]);

        String text = "You have sent me the following coordinates: " + latitude + ", " + longitude;

        return "{\"text\": \"" + text + "\", \"icon_url\": \"https://res.cloudinary.com/teepublic/image/private/s--R9uL4Dv7--/t_Resized%20Artwork/c_fit,g_north_west,h_954,w_954/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_bbbbbb,e_outline:3:1000/c_mpad,g_center,h_1260,w_1260/b_rgb:eeeeee/c_limit,f_jpg,h_630,q_90,w_630/v1499156483/production/designs/1712341_0.jpg\"}";
    }

    /**
     * Create a JSON payload with a Google Maps URL
     * @return the JSON payload
     */
    private String googleMap() {
        System.out.println("Sending Google Map location");

        return "{\"text\": \"https://www.google.com/maps?z=17&t=m&q=loc:46.520393+6.630652\", \"icon_url\": \"https://res.cloudinary.com/teepublic/image/private/s--R9uL4Dv7--/t_Resized%20Artwork/c_fit,g_north_west,h_954,w_954/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_ffffff,e_outline:48/co_ffffff,e_outline:inner_fill:48/co_bbbbbb,e_outline:3:1000/c_mpad,g_center,h_1260,w_1260/b_rgb:eeeeee/c_limit,f_jpg,h_630,q_90,w_630/v1499156483/production/designs/1712341_0.jpg\"}";
    }

    /**
     * Generate a text
     * @return the generated text
     */
    private String randomText() {
        switch (this.random.nextInt(5)) {
            case 1: return "\"text\": \"That still only counts as one.\",";
            case 2: return "\"text\": \"Home is behind, the world ahead... and there are many paths to tread... Through shadow... To the edge of night... Until the stars are all alight... Mist and shadow, cloud and shade... All shall fade... all... shall... fade.\",";
            case 3: return "\"text\": \"And thus it was. A fourth age of middle-earth began. And the fellowship of the ring... though eternally bound by friendship and love... was ended. Thirteen months to the day since Gandalf sent us on our long journey... we found ourselves looking upon a familiar sight. We were home. How do you pick up the threads of an old life? How do you go on... when in your heart you begin to understand... there is no going back? There are some things that time cannot mend... some hurts that go too deep... that have taken hold. Bilbo once told me his part in this tale would end... that each of us must come and go in the telling. Bilbo's story was now over. There would be no more journeys for him... save one.\nMy dear Sam. You cannot always be torn in two. You will have to be one and whole for many years. You have so much to enjoy and to be and to do. Your part in the story will go on.\",";
            default: return "";
        }
    }

    /**
     * Create a JSON payload for a message that is not know from the server
     * @return the JSON payload
     */
    private String defaultText() {
        switch (this.random.nextInt(3)) {
            case 1:
                return smallText();
            case 2:
                return normalText();
            default:
                return longText();
        }
    }
}
